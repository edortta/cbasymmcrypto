<?php
require_once "CBAsymmCrypto.php";

/**
 * Instancio a classe
 */
$asymm = new OpenSSL_Asymmetric();

/**
 * Caso não tenha um par de chaves, 
 * crio o mesmo e o guardo no arquivo asymm.json
 */
if (!file_exists("asymm.json")) {
  $keys=$asymm->createPair();
  $asymm->config($keys);
  file_put_contents("asymm.json", json_encode($keys, JSON_PRETTY_PRINT));
} else {
  $asymm->config(json_decode(file_get_contents("asymm.json"), true));
}

/**
 * Usando a chave, testo o algoritmo
 */
$original="Esta é minha frase de testes.";
$codif = $asymm->cypherB64($original);
$decodif = $asymm->decipherB64($codif);

echo "Original:\n$original\n".str_repeat("-",80)."\n";
echo "Codificada:\n$codif\n".str_repeat("-",80)."\n";
echo "Decodificada:\n$decodif\n".str_repeat("-",80)."\n";