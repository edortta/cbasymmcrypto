<?php

class OpenSSL_Asymmetric {
  /**
   * createPair
   *
   * Cria um par de chaves (pub, priv) para poderem
   * ser usados em criptogrtafia assimétrica
   *
   * @return array
   */
  public function createPair() {
    $ret = [];

    $config = [
      "digest_alg"       => 'sha512',
      "private_key_bits" => 4096,
      "private_key_type" => OPENSSL_KEYTYPE_RSA,
    ];
    $keyPair = openssl_pkey_new($config);

    $privateKey = NULL;
    openssl_pkey_export($keyPair, $privateKey);
    $ret['privateKey'] = $privateKey;

    $keyDetails       = openssl_pkey_get_details($keyPair);
    $publicKey        = $keyDetails['key'];
    $ret['publicKey'] = $publicKey;

    return $ret;
  }

  /**
   * config
   *
   * Configura esta classe com o par já criado
   * Pode ser o recuperado de um arquivo de
   * configuração, por exemplo
   *
   * @param  array $pair
   * @return void
   */
  public function config($pair) {
    $this->publicKey  = $pair['publicKey'] ?? false;
    $this->privateKey = $pair['privateKey'] ?? false;
  }

  /**
   * cypher
   *
   * Codifica um conjunto de caracteres usando a chave publica
   * configurada previamente
   *
   * @param  mixed $message
   * @return void
   */
  public function cypher($message) {
    $cypMessage = NULL;
    if ($this->publicKey) {
      if (preg_match_all('/-----BEGIN PUBLIC KEY-----(.*)-----END PUBLIC KEY-----/s', $this->publicKey, $output_array)) {      
        $keyFinal = "-----BEGIN PUBLIC KEY-----\r\n" . trim(rtrim(chunk_split($output_array[1][0]),"\r\n")) . "\n-----END PUBLIC KEY-----";
      } else {
        $keyFinal = $this->publicKey;
        $keyFinal = "-----BEGIN PUBLIC KEY-----\r\n" . trim(rtrim(chunk_split($keyFinal),"\r\n")). "\n-----END PUBLIC KEY-----";
      }

      openssl_public_encrypt($message, $cypMessage, $keyFinal);
    } else {
      throw new Exception("Undefined publickey on cypher", 1);
    }
    return $cypMessage;
  }

  /**
   * decipher
   *
   * Decodifica um conjunto de caracteres usando a chave privada
   * configurada previamente
   *
   * @param  mixed $cypMessage
   * @return void
   */
  public function decipher($cypMessage) {
    $message = NULL;
    openssl_private_decrypt($cypMessage, $message, $this->privateKey);
    return $message;
  }

  /**
   * cypherB64
   *
   * Codifica usando cypher() mas o resultado é acomodado
   * como base64. Isso facilita as coisas na hora de enviar
   * os dados sobre um JSON, por exemplo.
   *
   * @param  mixed $message
   * @return void
   */
  public function cypherB64($message) {
    return base64_encode($this->cypher($message));
  }

  /**
   * decipherB64
   *
   * Decodifica usando decipher() mas tomando como base
   * um string em Base64. Especialmente útil quando
   * se recebem dados em um arquivo JSON.
   *
   * @param  mixed $cypMessageB64
   * @return void
   */
  public function decipherB64($cypMessageB64) {
    return $this->decipher(base64_decode($cypMessageB64));
  }

}
