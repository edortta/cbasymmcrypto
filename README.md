# cbAsymmCrypto

Classe básica para criptografia assimétrica em CronosBAAS

## Objetivo

Esta classe é utilizada como base para geração e utilização de chaves de criptografia para troca de informações via webhook no ambiente CronosBAAS.